﻿/*NXuAssignment3.cs
 * Assignment 03: Tic-Tac-Toe
 * Revison History
 *     Naite Xu, 2018.11.26: Created
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NXuAssignment3
{
    public partial class Form1 : Form
    {
        enum PlayerType
        {
            None,
            Cross,
            Circle
        }

        //Checking players' type, even is X, odd is O
        public int players = 2;
        //Counting play turns
        public int playturns = 0;
        //Counting the winner for players and draws
        public int x = 0;
        public int o = 0;
        public int draw = 0; 

        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Creating new game
        /// </summary>
        void restartGame()
        {
            //Array of PictureBox for initialize the game 
            PictureBox[] pictureBoxes = {
                pictureBox1,
                pictureBox2,
                pictureBox3,
                pictureBox4,
                pictureBox5,
                pictureBox6,
                pictureBox7,
                pictureBox8,
                pictureBox9,
            };
            //Checking players' type, even is X, odd is O; Counting play turns
            players = 2;
            playturns = 0;
            
            //initialize the game 
            foreach (var pb in pictureBoxes)
            {
                pb.Image = null;
                lblStatus.Text = "Player 1 turn";
            }
        }
        /// <summary>
        /// Starting the Game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            restartGame();
        }
        /// <summary>
        /// The Play Zone; adding players when clicking the PictureBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Add(object sender, EventArgs e)
        {
            PictureBox playerPicture = (PictureBox)sender;
            //X or O can only be placed in a free slot
            if (playerPicture.Image == null)
            {
                if (players % 2 == 0)
                {
                    playerPicture.Image = playerCross.Image;
                    lblStatus.Text = "Player 1 turn";
                }
                else
                {
                    playerPicture.Image = playerCircle.Image;
                    lblStatus.Text = "Player 2 turn";
                }
                //Counting playes and turns
                players++;
                playturns++;
                //showing the draw message and the winner message
                if (theDraw() == true)
                {
                    draw++;
                    lblDraw.Text = "Draw: " + draw;
                    MessageBox.Show("Draw");
                }
                else
                {
                    theWinner();
                }
            }
        }
        /// <summary>
        /// Checking Draw at game end
        /// </summary>
        bool theDraw()
        {
            if (playturns == 9)
                return true;
            else
                return false;   
        }
        /// <summary>
        /// Checking the Winner at game end
        /// </summary>
        public void theWinner()
        {
            //The winning path
            PictureBox[] theWinningPath =
                {
                //Row 
                pictureBox1, pictureBox2, pictureBox3,
                pictureBox4, pictureBox5, pictureBox6,
                pictureBox7, pictureBox8, pictureBox9,
                //Column
                pictureBox1, pictureBox4, pictureBox7,
                pictureBox2, pictureBox5, pictureBox8,
                pictureBox3, pictureBox6, pictureBox9,
                //Diagonal
                pictureBox1, pictureBox5, pictureBox9,
                pictureBox3, pictureBox5, pictureBox7
            };
            //Checking the path
            for (int i = 0; i < theWinningPath.Length; i += 3)
            {
                if (theWinningPath[i].Image != null)
                {
                    if (theWinningPath[i].Image == theWinningPath[i + 1].Image && theWinningPath[i].Image == theWinningPath[i + 2].Image)
                    {
                        if (theWinningPath[i].Image == playerCross.Image)
                        {
                            x++;
                            lblXWin.Text = "Cross(X): " + x;
                            MessageBox.Show("The Winner is X");
                        }
                        else
                        {
                            o++;
                            lblOWin.Text = "Circle(O): " + o;
                            MessageBox.Show("The Winner is O");
                        }

                    }
                }
            }
        }
        /// <summary>
        /// The New Game Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNewGame_Click(object sender, EventArgs e)
        {
            restartGame();
        }
    }
}
